from oidc import oidc

oidc_application = oidc.OidcApplication()
created = oidc_application.create(
    applicationIdentifier="addfff62-0739-4b38-871d-397260267e96",
    displayName="oidc application",
    description="OpenID Connect support for kubernetes clusters at CERN",
    administratorsId="addfff62-0739-4b38-871d-397260267e96",
    managerId="addfff62-0739-4b38-871d-397260267e96")

print("Status code from API:")
print(created)