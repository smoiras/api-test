# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import requests
import json


class OidcApplication:
    """OpenID Connect support for kubernetes clusters at CERN

    This class creates and deletes applications through the API
    """

    def __init__(self):

        self.url = 'https://authorization-service-api-qa.web.cern.ch/api/v1.0/Application'
        self.payload = {}
        self.client_id = "openstack-magnum"
        self.secret = ""

    def create(self, applicationIdentifier, displayName, description, administratorsId, managerId):

        self.payload = {
            "applicationIdentifier": applicationIdentifier,
            "displayName": displayName,
            "description": description,
            "administratorsId": administratorsId,
            "managerId": managerId,
            "client_id": self.client_id,
            "secret": self.client_id
        }

        r = requests.post(self.url, data=json.dumps(self.payload))

        return r.status_code

        # More checks to be added

        # if r.status_code == 200:
        #     return True
        # else:
        #     return False

    def delete(self, applicationIdentifier):

        r = requests.delete(self.url + "/" + applicationIdentifier)

        return r.status_code

        # More checks to be added
        # if r.status_code == 200:
        #     return True
        # else:
        #     return False
