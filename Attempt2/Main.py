from oidc import oidc

oidc_application = oidc.OidcApplication()
created = oidc_application.create(
    applicationIdentifier="addfff62-0739-4b38-871d-397260267e96",
    displayName="oidc application",
    description="OpenID Connect support for kubernetes clusters at CERN",
    administratorsId="addfff62-0739-4b38-871d-397260267e96",
    managerId="addfff62-0739-4b38-871d-397260267e96",
    identityId="3fa85f64-5717-4562-b3fc-2c963f66afa6",
    ownerId="3fa85f64-5717-4562-b3fc-2c963f66afa6",
    administratorsAccess=None,
    homePage="testtest.ch",
    resourceCategory=None,
    reassignable=True,
    autoReassign=True,
    blocked=False
)

print("Status code from API:")
print(created)